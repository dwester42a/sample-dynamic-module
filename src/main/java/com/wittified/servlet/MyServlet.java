package com.wittified.servlet;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.atlassian.pocketknife.api.lifecycle.modules.ModuleRegistrationHandle;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.dom4j.tree.DefaultElement;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MyServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(MyServlet.class);


	private final PluginAccessor pluginAccessor;
	private final DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory;
	private final WebInterfaceManager webInterfaceManager;

	private final Map<String, ModuleRegistrationHandle> moduleRegistrationHandleMap = new HashMap<String, ModuleRegistrationHandle>();


	public MyServlet(final DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory,
					 final WebInterfaceManager webInterfaceManager,
					  final PluginAccessor pluginAccessor)
	{
		this.dynamicModuleDescriptorFactory = dynamicModuleDescriptorFactory;
		this.pluginAccessor = pluginAccessor;
		this.webInterfaceManager = webInterfaceManager;
	}

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
		Plugin thisPlugin = this.pluginAccessor.getPlugin("com.wittified.dynamic");

		if( this.moduleRegistrationHandleMap.containsKey("wittified-panel-blah"))
		{
			this.moduleRegistrationHandleMap.get("wittified-panel-blah").unregister();
		}
		if( req.getParameter("unload")==null)
		{

			Element element = new DefaultElement("web-panel");
			element.addAttribute("key", "wittified-panel-blah");

			element.addAttribute("name", "wittified-panel-blah");
			element.addAttribute("location", "atl.header");
			element.addAttribute("weight", "99999");
			element.addElement("resource").addAttribute("name", "view").addAttribute("type", "static").addText(
					"<div>Hi</!div>");

			this.moduleRegistrationHandleMap.put("wittified-panel-blah", this.dynamicModuleDescriptorFactory.loadModules(thisPlugin, element) );
		}
		this.webInterfaceManager.refresh();
		resp.setContentType("text/html");
		resp.getWriter().write("<html><body>Processed</body></html>");
    }

}